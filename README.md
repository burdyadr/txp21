# TX GI Étude d’impact de la couche web cliente et éventuelles préconisations pour un site web frugal

## Sujet

1. Question principale : rechercher des économies sur la couche cliente web a-t-elle du sens ? (La question n’est donc tant pas de savoir si on peut faire des économies, mais si ces économies sont “significatives”).
2. Question secondaire : si on peut faire des économies significatives, proposer des pistes pratiques pour les réaliser.

- État de l’art (superficiel) des études d’impact existantes pour positionner la partie consultation web (Ademe, Shift-project…).
- État de l’art d’études d’impact de diverses pratiques liées au développement web (contenus tiers, CDN, image/audio/vidéo, compression, BDD, technos serveurs…) pour servir de référence et orientée les choix.
- État de l’art et méthodo : trouver des indicateurs et une méthode de mesure de coût en ressources d’une page web.
- Proposer un corpus d’étude (contenus libres autorisés à la modification, accès aux administrateurs des sites) : Aswemay (pure web, peu interactif), Librecours (pure web, applicatif JS important), Wikipédia (wiki avec BDD, et version pure web), Framablog (CMS avec tiers applicatif, Wordpress), FSF?, April?, Reporterre?, service-public.fr…
- Expérience 1 :
    - Proposer une version alternative HTML(/CSS/JavaScript) la plus frugale possible en terme de consommation de ressources lors de la consultation, tout en permettant de rendre le service (segmenter au besoin, exemple des commentaires).
    - Mesurer le gain de chaque simplification (exemple : la réduction du CSS de Xko à Yko permet d’économiser ceci).
    - L’analyser au regard de données globales de consommation de ressources web, pour évaluer s’il est significatif et dans quelles proportion (par rapport aux autres postes de consommation web images/audio/vidéos, réseau…, par rapport aux autres postes logiciel, par rapport aux postes matériels).
- Expérience 2 (utilité à qualifier, c’est une sorte d’expérience 1 poussée à la limite et avec perte fonctionnelle) :
    - Proposer une publication Gemini (https://fr.wikipedia.org/wiki/Gemini_(Protocole)
    - Énoncer et qualifier les pertes fonctionnelles
    - Mesurer le gain
    - Qualifier s’il est significatif ou pas
- Pistes complémentaires :
    - comparer avec d’autres approches frugales by design (état de l’art à faire)
    - on reste a priori sur la phase d’utilisation, mais à voir si le cycle de vie de création/maintenance est à considérer
- Préconisations pour un site web frugal
    - Hypothèses de bonnes et mauvaises pratiques de développement web (c’est à dire dont on pense qu’elles sont économes ou pas)
    - Indicateurs et protocoles de mesure (construire des jeux de test et mesurer)
    - Critique de chacune des bonnes et mauvaises pratiques (réfutation, mise en contexte, validation par l’expérience) ; mise en forme de fiches de réfutabilité
    - Prototypage de redesign de sites existants sous CC à des fins de test de “ce que ça donnerait” (Wikipédia, FSF, Framasoft…)

## Outils

- Product backlog : https://kanban.picasoft.net/b/wweKNbLNCQXPvN4kQ/tx-p21-web-client

- Fiche de temps : https://lite.framacalc.org/9m8w-txwebclientp21

- Espace de publication : https://wwwetu.utc.fr/~burdyadr/

## GitLab
Ce répertoire Git nous sert à héberger nos codes et autres documents produits et étudiés au cours du projet.
Il fonctionne en sous-dossier selon la logique suivante :
- DOC : Dossier contenant l'ensemble des documents externes au projet susceptibles de nous aider au cours de notre recherche et de notre état de l'art. Il contient le dossier FDL où sont entreposées les fiches de lectures que nous avons produites.
- PUB : Dossier contenant l'ensemble des codes destinés à être public.
- EXP : Dosser contenant les expériences réalisées et leurs résultats.

## Itérations

ODJ et CR : https://pad.picasoft.net/p/TXWebClient1


