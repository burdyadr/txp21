# Fiche de lectures sur le positionnement de la couche cliente et de ses usages principaux

## Déployer la sobriété numérique : résumé aux décideurs, The Shift Project, Octobre 2020 (1)

* "la croissance de nos systèmes numériques est insoutenable – +9% d’énergie  consommée par an –"

* Distribution de la **consommation d'énergie finale** du numérique par poste
	* Utilisation (55%)
		* Terminaux (20%)
		* Data centers (19%)
		* Réseaux (16%)
	* Production (45%)
		* Ordinateurs + Smartphones (28%)
		* TVs + Autres (17%)

* La sobriété passe peut-être par l'usage qu'elle induit :
	* Le bilan énergétique net n’est souvent positif que si les com-portements en phase d’utilisation sont orientés par une gou-vernance alignée sur les objectifs d’économie d’énergie.
	* "Nos usages numériques se construisent aujourd’hui autour d’automatismes, de designs de captation de l’attention et de modèles économiques rendant profitable la consommation continue de contenus rendus omniprésents"

## Green IT : quel est le vrai impact du numérique sur l'environnement ?, Bpifrance Le Hub (Ubsek & Rica), 8 mars 2021 (2)

* [Fabrication + Usage + Fin de vie] des terminaux = 85% (du bilan GES) & Reseau + Serveur (stockage) = 15% (du bilan GES)

* Fabrication des terminaux = 70 à 80 % de [Fabrication + Usage + Fin de vie] des terminaux.

* -> 13 à 21% (du bilan GES) pour l'usage des terminaux par rapport à tout l'usage du numérique en général, cela reste non négligeable ( d'après des déductions de Sylvain Spinelli https://team.picasoft.net/tx-stph/pl/ow4q8i1rkffo3x7gbpgs1ssiao)

## Empreinte environnementale du numérique mondial, GreenIT.fr Frédéric Bordage, Septembre 2019 (3)

* "On  a  schématiquement  la  hiérarchie  suivante  pour  les  sources  d’impacts,  par  ordre  décroissant :
	1. Fabrication des équipements utilisateurs ;
	2. Consommation électrique des équipements utilisateurs;
	3. Consommation électrique du réseau ; 
	4. Consommation électrique des centres informatiques ;
	5. Fabrication des équipements réseau ;
	6. Fabrication des équipements et des centres informatiques (serveurs, etc.).
* Dans le monde en 2019 :
	* 30% de la consommation d'énergie primaire provient de l'usage des terminaux
	* 26% des émissions de GES provient de l'usage des terminaux
	
## Impacts environnementaux du numérique  en France, Frédéric Bordage, Lorraine de Montenay, Olivier Vergeynst, 17 janvier 2021 (4)

* En France en 2020 :
	* 27% de la consommation d'énergie provient de l'usage des terminaux
	* 8% des émissions de GES provient de l'usage des terminaux
	* 5% de la consommation d'eau douce potable et 0% de la consommation de matières premières sont dûs à l'usage des terminaux

* Phase d'usage : 65% de la consommation d'énergie primaire (56% du bilan GES)

* Phase de fabrication : 35% de la consommation d'énergie primaire  (44% du bilan GES)

## CLIMATE CRISIS:THE UNSUSTAINABLE USE OF ONLINE VIDEO, The Shift Project, Juillet 2019 (5)

> 10 hours of high definition video comprises more data than all the articles of Wikipedia in English in text format

data traffic = 50% de la consommation d'énergie

> Video flows represented 80% of global data flows in 2018

20% restants : jeux vidéo, sites web (en remontant à la source càd Savine report : 13,1% of GLOBAL APPLICATION CATEGORY TRAFFIC SHARE pour le web en 2019)

## Environmental impact assessment of online advertising, M. Pärssinena, M. Kotilab, R. Cuevasc, A. Phansalkard, J. Manner, 2018 (6)

* Consommation d'énergie de la publicité en ligne : 20 à 282 TWh (2016)
* Consommation d'énergie l'infrastructure globale : 791 à 1334 TWh (2016) (1400 en 2019 d'après GreenIT.fr)
* Emissions de GES de la publicité en ligne : 60 Mt CO2e (1400 Mt CO2e pour tout le numérique d'après GreenIT.fr)
	* 1,5 à 35,7% de la consommation dûe à la publicité
	* 4,3% des émissions dûes à la pub

## IMPACT ENVIRONNEMENTAL DU NUMÉRIQUE : TENDANCES À 5 ANS ET GOUVERNANCE DE LA 5G, The Shift Project, 2021 (7)
* plus de 5% de la consommation d'énergie primaire mondiale est liée au numérique
* 3,5% = part du numérique dans les émissions de GES mondiales en 2019 (2,9% en 2013) soit 1,84 GtCO2eq
	* taux de croissance + 5,5 %/an entre 2015 et 2019

Distribution de l'empreinte carbone du numérique mondial par poste en 2019 :
* Utilisation (63%)
	* Terminaux utilisateurs (38%)
	* Réseaux (11%)
	* Centre de données (14%)
* Production (39%)
	* Ordinateurs et smartphones (10+10=20%)
	* TV et autres (19%)



## Quelles questions se pose-t-on et auxquelles peut-on répondre ?

### Usage
Un site frugal c'est un site :
1. Qui consomme moins de ressources systèmes sur le terminal
	1. Quelle est la consommation "normale" ?
	2. Quelles sont les économies possibles ?
2. Qui fait transiter moins de données sur le réseau : 
	1. Quelle est la consommation du réseau ? 
	2. Quelles sont les économies possibles ?
3. Qui fait stocke moins de données sur un serveur
	1. Quelle est la consommation "normale" des serveurs ?
	2. Quelles sont les économies possibles ?
- Critères : énergie finale/primaire, émissions de GES liées à la production de cette énergie, consommation d'eau douce

### Fabrication
Un site frugal c'est un site :
1. Qui prolonge la durée de vie des terminaux
	1. Quelle est la durée de vie "normale ?
	2. De combien peut-on prolonger ?
2. Qui prolonge la durée de vie du réseau
	1. Quelle est la durée de vie "normale" du réseau ? 
	2. De combien peut-on prolonger ?
3. Qui prolonge la durée de vie des serveurs
	1. Quelle est la durée de vie "normale" des serveurs ?
	2. De combien peut-on prolonger ?
- Critères : consommation d'eau douce, consommation de ressources abiotiques (métaux), émissions de GES

**Quels infos a-t-on sur ces questions ? Quel positionnnement en déduire ?**

## Résumé

Deux découpes possibles : **Fabrication/Usage** ou **Equipements utilisateurs/Réseau/Serveurs**

### Usage
* 55% de la consommation d'énergie finale est liée à l'utilisation (1)
* 65% de la consommation d'énergie primaire est liée à l'utilisation (3)
* 56-63% des émissions de GES sont liées à l'utilisation (3, 7)
### Fabrication
* 45% de la consommation d'énergie finale est liée à la fabrication (1)
* 35% de la consommation d'énergie primaire est liée à la fabrication (3)
* 39-44% des émissions de GES sont liées à la fabrication (3, 7)
* 100% de la consommation de ressources abiotiques (3)
* 79% de la consommation en eau douce (3)

___

### Equipements utilisateurs
* 60% de l'énergie primaire totale du numérique (3)
	* **30%** de l'énergie primaire totale du numérique liée à **l'utilisation des équipements utilisateurs** 
* 63% des émissions de GES totales du numérique(3)
	* **26%** des émissions de GES totales du numérique liée à **l'utilisation des équipements utilisateurs** / 39% selon (7)
* 83% de la consommation en eau douce (3)
* 75% de la consommation de ressources abiotiques (3)
### Réseau
* 23% de l'énergie primaire (3)
	* **20% de la consommation d'énergie primaire est liée à l'utilisation du réseau**
* 22% des émissions de GES (3)
	* **16% des émissions de GES sont liées à l'utilisation du réseau**
* 32% de la consommation en eau douce (3)
* 16% de la consommation de ressources abiotiques (3)

Flux :
* 80% du flux de données est lié à la vidéo (5)
* 13,1% du flux de données est lié au web (5)

### Serveurs
* 17% de l'énergie primaire (3)
* 15% des émissions de GES (3)
* 7% de la consommation en eau douce (3)
* 8% de la consommation de ressources abiotiques (3)
___

### Publicité
* 1,5 à 35,7% de la consommation liée à la publicité
* 4,3% des émissions de GES liées à la publicité

### Conclusions :
* Sur le réseau, si on suppose que les impacts sont proportionnels au flux, on aurait `0,131*0,16=0,021` soit **2,1% du GES du numérique lié à l'utilisation du réseau pour le web**
* La couche cliente est responsable de 26% du GES du numérique lors de son utilisation (au maximum)
	1. Quelle part de la consommation d'un terminal est liée au navigateur ? (mesurer la part de CPU ?)
	2. Est-ce que la répartition des flux de données peut être appliquée à la consommation de la couche cliente ? Si oui, `0,131*0,26=0,341` ou `0,131*0,38=0,05` soit **3,4 à 5% du GES du numérique lié à l'utilisation des équipements utilisateurs pour le web**
	*	

Pour répondre à la question principale de la TX : 
> rechercher des économies sur la couche cliente web a-t-elle du sens ? 

il faut approfondir ces points :
* Mesurer la consommation à vide d'un ordinateur et avec le navigateur pour estimer la part sur laquelle on peut agir ?
* La fabrication des équipements utilisateurs a un impact très important, notamment sur les ressources abiotiques et l'eau douce. Comment estimer les impacts indirects des économies sur la couche cliente sur l'obscolescence matérielle ?