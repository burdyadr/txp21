## DU LOW-TECH NUMÉRIQUE AUX NUMÉRIQUES SITUÉS

**Nova & Roussilhe**

L’injonction à l’intégration des techniques numériques dans les organisations et activités quotidiennes se heurte à la crise environnementale systémique. Plusieurs propositions se mettent en place pour tenter de dépasser cette opposition, dont l’orientation « low-tech ». L’article ci-résumé s’intéresse à l’écho que peut avoir cette mouvance chez les designers du numérique. Face à l’absence observée d’une définition consensuelle, les auteurs se proposent d’interroger la notion de low-tech à l’aune des différentes pratiques qui s’y réfèrent.

### 3. Design web à basse consommation

Le questionnement autour des réductions d’émissions d’équivalent carbone traverse le milieu du design numérique.

En premier lieu, les outils de mesures, malgré leurs biais méthodologiques et statistiques – « comme la disparité du calcul de l’énergie nécessaire au transit d’un octet (Coroama et Hilty, 2014) ».
- Website Carbon (site) : « fournit une empreinte carbone pour un trafic donné et des équivalences en kilogrammes de CO2 »
- Carbonalyzer (plug-in) : « a consommation électrique et les émissions liées à un flux de données »

Ensuite les outils de description de pages web, de leurs éléments et de la mise en cache, « la complexité des pages invitant à modifier la conception et le développement des sites web »
- Ecoindex & Web Energy Archive

Enfin, les outils de génération de sites et de solutions de gestion de contenu.
- tkti : création de blogs légers
- Thème Wordpress de Jack Lenox (https://sustywp.com)
- Cold CMS

Ces nouveaux outils modifient les méthodes de conception et le périmètre d’action des designers numériques, ils rendent plus accessibles les approches d’éco-conception.

### 6. Numériques situés

Face à une notion de low-tech jugée trop vague, et problématique – car en opposition binaire avec le high-tech, alors que la réalité donnée à voir par l’article laisse place à une multiplicité des approches –, les auteurs proposent l’adoption du terme de numérique situé.
Cette nouvelle vision s’articulerait autour de 3 points :
- Matérialiser les infrastructures et des impacts environnementales du numérique ;
- Territorialiser les infrastructures numériques et leurs services en prenant en compte les spécificités de leur territoire d’implantation ;
- Terrestrialiser les objectif écologiques : les réductions d’impact visées doivent inclure et dépasser le territoire où ces innovations prennent place.

**Intérêt de l’article pour notre corpus d’étude :** pourrait proposer une piste critique sur la notion de numérique low-tech et mettre en avant que si la couche client peut être un espace d’étude légitime, les impacts et pistes d’amélioration s’inter-opèrent à tous les niveaux. Propose également des outils autours de l’éco-design web.

## How to Build a Low-tech Website?

### Pourquoi un site web low-tech ?

Dans la recherche pour un Internet plus "vert", l'idée d'investir dans les énergies renouvelables est récurrente. Or, la demande énergétique d'Internet excède aujourd'hui d'un facteur 3 la quantité d'énergie que l'ensemble des sources éoliennes et solaires du monde peuvent produire.

Ces énergies renouvelables ne sont par ailleurs pas constamment disponibles. Dans la quête pour un Internet plus sobre, il est alors nécessaire de voir plus loin que les moyens de répondre aux besoins énergétiques en s'interessant de manière plus directe à cette demande en énergie, en perpétuelle augmentation.

En effet, alors qu'Internet fut longtemps présenté comme un moyen de dématérialiser la société et de diminuer les quantités d'énergies (Cf. [SMARTer 2020: The Role of ICT in Driving a Sustainable Future](https://www.bcg.com/publications/2012/energy-environment-technology-industries-smarter-2020-role-ict-driving-sustainable-future)), on constate aujourd'hui que celui-ci fait partie des milieux techniques les plus consommateurs d'énergie (8% de l'electricité globalement produite dans le monde d'après [Why We Need a Speed Limit for the Internet](https://solar.lowtechmagazine.com/2015/10/can-the-internet-run-on-renewable-energy.html)).

L'article identifie quelques raisons pour expliquer cet effet rebond :

* > The size of the average web
page (defined as the average page size of the 500,000 most popular domains) increased from 0.45 megabytes (MB) in 2010 to 1.7 megabytes in June 2018. For mobile websites, the average “page weight” rose tenfold from 0.15 MB in 2011 to 1.6 MB in 2018.

* L'augmentation du traffic réseau excède les avancées techniques d'efficacité énergétique ;

* L'augmentation de la taille des pages web réduit la durée de vie des machines et accroit donc la quantité de machines à produire ;

* Augmentation du temps d'usage du numérique lié à l'augmentation des moyens d'accès à Internet.

* Paradigme du "always-on" Internet

### Low-tech Web Design

Pour le Low <- Tech Magazine, l'adoption d'un design low-tech à diminué d'un facteur 5 la taille du site.

Cf. le document technique dédié aux spécifications front-end.

* Adoption d'un site statique hébergé que le disque dur d'un serveur, sans compilation ou accès à une BDD pour le générer ;

* Diminution d'un facteur 10 de la quantité de ressources requise pour les images par "Dithering" (noir et blanc) et ajout de coloration par les capacités de manipulation d'image des navigateurs ;

* Police d'écriture du navigateur par défaut et absence de logo (l'identité graphique passe par le nom de site et les symboles affichables) ;

* Pas d'usage de cookies, de service de publicité et de tracker tierces. L'auto-herbergement permet d'obtenir des logs anonymisés permettant de faire des mesures sans passer par des outils comme Google Analytics. Usage de solutions de financement alternatives (financement participatif, publication physique des articles)  

La mise en place d'un site fonctionnant aux énergies renouvelables nécessite de penser le temps d'autonomie énergétique du serveur. Aujourd'hui, le Low<-Tech Magazine estime être hors-ligne en moyenne 35 jours par an.

### Caractéristiques "techniques" :
> SERVER: This website runs on an Olimex A20 computer. It has 2 Ghz of processing power, 1 GB of RAM, and 16 GB of storage. The server draws 1 - 2.5 watts of power.

> SERVER SOFTWARE: The webserver runs Armbian Stretch, a Debian based operating system built around the SUNXI kernel. We wrote technical documentation for configuring the webserver.

> DESIGN SOFTWARE: The website is built with Pelican, a static site generator. We have released the source code for ‘solar’, the Pelican theme we developed here.

> INTERNET CONNECTION. The server is connected to a 100 MBps fibre internet connection. Here’s how we configured the router. For now, the router is powered by grid electricity and requires 10 watts of power. We are investigating how to replace the energy-hungry router with a more efficient one that can be solar-powered, too.

> SOLAR PV SYSTEM. The server runs on a 50 Wp solar panel and one 12V 7Ah lead-acid battery. However, are still downsizing the system and are experimenting with different setups. The PV installation is managed by a 20A solar charge controller.

**Intérêt de l’article pour notre corpus d’étude :** Exemple concret d'un site web visant la sobriété énergétique. S'il manque de chiffres précis et quantifiés, les auteurs renvoyant à des wikis spécifiques pour ces enjeux, cet article illustre cependant en partie le "niveau 2" de la *low-technicisation* par une remise en question des usages pour rapprocher ces derniers du strict nécessaire.

## How to Build a Low-tech Website: Design Techniques and Process

Cet article/wiki, écrit par Low-Tech Magazine s'intéresse aux efforts front-end effectués par le site dans l'objectif de produire un site internet soutenable à la consommation énergétique minimale. La première partie, faisant l'objet de cette fiche, s'axe sur les techniques de design. Une seconde partie dédiée au processus de génération d'un site web statique est à venir. Elle n'a pas encore été publiée.

**Limite principale :** L'article donne les décisions prises mais aucune quantification des économies obtenues. Une possibilité pour la suite de la PR pourrait être de reproduire certains choix du site pour en mesurer les effets.

### Part 1 : Design

L'enjeu de cette partie est d'identifier quels éléments graphiques et fonctionnalités superflus peuvent être enlevés sans nuire au fonctionnement du site.

D'après l'article, ce positionnement est central car le design d'un site web représenterait la majorité de son usage énergétique. Cette assertion s'appuie sur un livre de Tim Frick intitulé "Designing for Sustainability", en citant le passage suivant :

> "[F]rontend components—JavaScript, images, CSS/ HTML, and other assets, plus page rendering tasks a browser must perform—comprised between 76 to 92% of total page load time. Because design decisions drive so much of what happens on the frontend, designers clearly have a critical role to play in creating optimized solutions."

### Logo

* Recours à un logo typographique (du texte) plutôt qu'une image.


### Default typeface

* Les polices personnalisées impacteraient la performance en augmentant le temps de chargement des pages en remplaçant / surchargeant le style défini par défaut par les navigateurs.

### Images  

#### Dithering :
* conversion des images en 4 niveaux de gris par un plug-in customisé sous Pelican pour obtenir des pages d'environs 1MB tout en gardant l'avantage d'avoir des images dans les articles (meilleure attractivité, pause visuelle et consistence de la charte graphique).
D'après cet [hyperlien](https://homebrewserver.club/low-tech-website-howto.html#image-compression) plus détaillé :
> Using this custom plug-in we reduced the total weight of the 623 images that are on the blog so far by 89%. From 194.2MB to a mere 21.3MB

* La possibilité d'étirer des images "ditherée" à été utilisée comme partie prenante du design dans la mesure où, au lieu de multiplier le nombre de copies d'une même images dans différents formats en fonction de l'usage (comme illustration d'article ou vignette sur la page principale, sur différentes configurations d'écrans...), la même image est utilisée dans différents contextes et optimise ainsi le recours au cache du navigateur.

* La coloration des images que le site final est réalisée par l'intermédiaire des *CSS blend-modes*.

> For background images, we can use the background-color. For content images in articles, we will need it in a wrapper (coloring the img background does not work.) Fortunately, the format in which the articles are written – in markdown — automatically wraps images specfied in a paragraph tag. These image-wrapping paragraphs were captured via the addressable a Pelican plugin in order to colorize the images.

#### SVG :

Le recours à du SVG inline permet d'éviter les demandes de chargement d'images. Cette méthode permet également de styliser l'image via des classes CSS. L'ensemble des icônes en mode hover est implémenté en SVG inline.

#### Image Sprites :
- minimiser le nombre de requêtes serveur en combinant de plusieurs petites images en une seule.
- > Storage-wise, six image files (150 x 150 each) totalled 9KB, whereas the combined image (150 x 900) is a 6KB file that only loads once

### JS  
- Ecriture de JS natif lorsque cela s'avérait nécessaire pour éviter la dépendance à des librairies extérieures (de manière similaire aux images, cela augmenterait le nombre de requêtes vers le serveur).
- Scripts limités à la page qui les utilise pour ne les charger que lorsque cela s'avère nécessaire.

### Site Printer-Friendly
L'agencement des articles est fait pour favoriser leur impression sur papier en maximisant l'espace occupé par le texte sur la page. Au-delà d'un temps de lecture certain, il est moins "polluant" d'imprimer et de lire sur papier que de rester sur un format numérique en ligne.

## Le guide d’éco-conception de services numériques

* Guide de l'association "Designers éthiques" dédié à recenser "les principales bonnes pratiques de design pour réaliser des services numériques à l’empreinte environnementale réduite".
* Part du principe que l'éco-conception est à mettre en lien avec les questions d'accessibilité et d'expérience utilisateur.
* **Ecoconception :** "démarche d’amélioration continue qui vise à limiter les ressources informatiques et énergétiques au niveau du terminal utilisateur, du réseau et du centre informatique"

### 1. Définir le besoin et éliminer ce qui n’est pas essentiel

> *Plus on intervient tôt, c’est à dire lors de l’expression du besoin, de la conception fonctionnelle, du maquettage, plus l’effet de levier est fort en terme de réduction de l’empreinte environnementale.* - Frédéric Bordage, Ecoconception / les 115 bonnes pratiques

* Évaluer précisément les besoins permet de se concentrer sur l'utilité fonctionnelle du service en travaillant surtout sa fonction principale.
* Avec [1], "plus le besoin exprimé est sobre (voire frugal) et prend en compte les impacts RSE, plus la conception responsable est facilitée et plus le produit sera efficient".

### 2. Evaluer et mesurer

* Pour un site web déjà existent : "évaluez l’impact environnemental du parcours utilisateur et identifiez ses axes d’amélioration et les bonnes pratiques à mettre en place" ;
* Pour un nouveau service : "évaluer un service concurrent ou similaire pour éviter les mêmes erreurs. Mesurez l’impact de l’expérience sur des sites équivalents. Tentez d’évaluer votre nombre d’utilisateurs, le matériel nécessaire et l’impact de votre service du mieux possible pour orienter vos choix ultérieurs." (ex : *Que se passe-t-il si 100 millions de personnes utilisent votre service ?*) ;

#### Evaluer l’impact du parcours utilisateur

* Prérequis : unité fonctionnelle et scénario d'utilisation ;
* Possibilité de mesure avec GreenIT Analysis en analysant et sauvegardant chaque page pour obtenir un score total. L'outil propose aussi une analyse des bonnes pratiques.
* Limite : outil fournissant une première approche, ne remplace pas une ACV.

### 3. Simplifier le parcours et fluidifier l'expérience

> "Plus un utilisateur passe de temps sur un site pour accomplir son objectif, plus l’empreinte environnementale sera élevée."

* Lien entre éco-conception et économie de l'attention : les pratiques de design et de développement web étant largement influencées par les modèles des GAFAM, il est important de questionner les tendances de conception. Cela passe par une définition de ce qui est nécéssaire au projet.

### 4. Concevoir en “mobile first”

* Commencer par une application desktop implique d'adapter le contenu en mode "responsive" pour les téléphones mobiles. Expérience détériorée et chargement de contenu superflus ;
* Approche "mobile first" : aller à l'essentiel donc fournir des services plus sobres, pour ensuite se concentrer sur la version desktop.

### 5. Réaliser et développer

Regroupe un ensemble de bonnes pratiques de conception.

#### 5.1. Les images

* Type de ressources le plus utilisé sur le web d'après [2] ;

* > Le poids d’une page moyenne sur desktop serait de 2062 Ko sur l’échantillon testé, et le poids moyen des images transférées pour une page serait de 973.4 Ko ;

* Une bonne pratique : redimensionner les photos et images à la taille d'une page web pour en réduire le poids et éviter au navigateur de la redimensionner. Penser à diminuer la qualité également ;

* Préférer les images vectorielles et glyphes car moins lourdes que les photos ;

* Choisir des métaphores fidèles pour désigner des actions auprès des utilisateurs ("“se connecter à un autre ordinateur” plutôt que de “mettre dans le cloud”") ;

* Compresser les images ;

* Servir les images selon la taille de l'écran en fournisssant plusieurs tailles et en ayant recours à la balise html '''<picture>''' ;

#### 5.2. Les vidées et le son

* D'après "Sobriété numériques : Les clés pour agir" de Frédéric Bordage, " l’usage de la vidéo en ligne représente entre 60 % et 90% du trafic internet" ;

* Eviter les fonds vidéos et l'autoplay : alourdissement de la page, captation attentionelle, manque d'accessibilité.

* Compresser les vidéos et les sons en fonction des usages ;

#### 5.3. Les animations

* Animations nécessitent des appels serveur et des ressources ;

* Permettre aux utilisateurs d'arrêter les animations ;

* Préférer les changements instantanés plutôt qu'animés ;

#### 5.4. Les polices

* Avoir recours aux polices pré-installées dans le terminal pour réduire l'usage de la bande passante et accélérer le chargement du site ;

* Privilégier le format WOFF2, plus compressé que le format WOFF pour les navigateurs récents ;

#### 5.5. Les plugins et widgets

#### 5.6. Le contenu

* Opter pour une rédaction claire et concise privilégiant les listes à puce aux blocs de texte ;

* Faciliter la navigation par l'usage de feuilles de style et de plusieurs méthodes (menu, plan du site, moteur de recherche) ;

* Ne pas justifier le texte car "l’espacement variable entre les mots peut gêner la lecture pour les personnes dyslexiques."

#### 5.7. Les interactions

* Remplacer le scroll infini augmentant le temps passé sur la page (processus de captologie) et contribuant à augmenter les ressources techniques nécessaires.

* Eviter la complétion automatique qui nécessite beaucoup de requêtes vers le serveur ;

* Interroger l'utilité et la fréquence des notifications ;

#### 5.8. Les documents à télécharger

* Ne se voit pas dans le poids de la page, mais devrait quand même être pris en compte ;

* Optimiser la résolution et compresser les documents ;

* Fournir un résumer pour éviter le téléchargement inutile ;

#### 5.9. Les options par défaut

* Options les plus utilisées donc ayant le plus d'impacts. Exemple de bonne pratique : proposer par défaut au téléchargement une qualité optimisée et en option les qualités supérieures.

### 6. Tester, évaluer, et maintenir

* Tester un même service avec différentes connexions (Wi-Fi, 2G, 3G...) ;

* Evaluer l'impact du parcours utilisateurs, l'effet de la mise en place de bonnes pratiques et l'accessibilité ;

* Penser l'évolution du service et des contenus en formant les personnes qui reprendront le projet, en triant le contenu déjà publié, quitte à supprimer des choses.

### Aller plus loin dans la démarche

* Penser des modes de consultation hors ligne si celui-ci réduit le transfert de données (ex : les Progressive Web Apps) ;

* Dé-numériser les besoins utilisateurs lorsque cela est possible

La fin du chapitre est dédiée à donner d'autres sources pour aller plus loin dans le design éco-conçu.

**Intérêt du rapport pour notre corpus d’étude :** Revue assez complète et illustrée de la démarche de design éco-conçus avec beaucoup de ressources. Offre une vision synoptique des solutions et propose quelques chiffres intéressants. 

### Sources liées :
[1] – ["Quand l’expression de besoins devient responsable", Philippe Derouette – 11/2020](https://institutnr.org/lexpression-de-besoins-responsable)

[2] - ["Page Weight", http archive](https://httparchive.org/reports/page-weight)
