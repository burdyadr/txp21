# Fiches de lectures



## Green Patterns : Manuel d'éco-conception des logiciels, Green Code lAB, 2019

* "La    couche    logicielle    est    le    principal    responsable    de l’obsolescence    accélérée    des    ordinateurs,    des    tablettes numériques  et  des  smartphones.  Essentiellement  à  cause  des besoins  en  ressources  (mémoire  vive,  cycles  CPU,  espace disque, etc.) des nouvelles versions de logiciels qui augmentent constamment"

* Phase d'utilisation : 
	* "L’énergie consommée « instantanée » lors de l’utilisation du logiciel par le matériel : énergie de l’ordinateur (CPU, disque, RAM, périphériques), du ou des serveurs, du réseau"
	* "L’énergie  et  les  matériaux  consommés  pour  produire  le matériel (ordinateurs, serveurs, réseaux, périphériques) qui permet d’exécuter le logiciel avec sa quote-part d’utilisation et  d’usure  sur  la  base  d’un  nombre  d’années  d’utilisation."
	* L’utilisation   de   consommables   par   le   logiciel   (papier, encre...).

*(à poursuivre pour la partie préconisations)*

### Chapitre 2
Interroge l’impact direct du code sur l’environnement. mise en situation sur le logiciel et le dd.
« Les logiciels sont même la principale cause de l’obsolescence accélérée des équipements informatiques. Et donc, indirectement, la principale cause de l’augmentation continue de l’empreinte écologique de l’informatique. »

* Scénario entre points par ONU : 
	* Efficience (techniques plus performantes)
	* Sobriété (techniques utilisées avec parcimonie
	* Utilisation de ressources renouvelables
* Appliqué aux TIC :
	* Réduire l’empreinte des tic
	* Réduire l’empreinte de l’organisation grâce aux tic
	* Inventer de nouveaux produits et services, plus durables grâce aux tic

#### Quel impact du développement logiciel (p 36)
* « L’empreinte environnementale des TIC est liée principalement à la fabrication et à la fin de vie du matériel. » Pourquoi renouvelons-nous prématurément le matériel informatique ? Principal responsable de l’obsolescence accélérée est la couche logicielle. 
* Chaque nouvelle version nécessite en moyenne 2 fois plus de puissance (= de matériel). Il faut 70 fois plus de mémoire vive sous office 2010 pour écire le mêm texte que sous office 97.
* Cloud computing: s’il devrait permettre de mutualiser le matériel et ainsi réduire l’empreinte éco d’un service, il ne permet en fait qu’un effet rebond, et une augmentation des « besoins » (plutôt attentes) 


* Lois de Lehman : 
	* Un logiciel doit constamment être adapté
	* La qtructure se complexifie au fur et a mesure du temps
	* Le contenu doit augmenter pour satisfaire les « besoins » des users

* Logiciel et matériel sont liés par phénomène appelé obésiciel (ou bloatware) = baisse de perf des systèmes informatiques entre des versions successives d’un logiciel aka pour une même fonctionnalité, on perd en performances.
Causes : 
	* Parasitage : logiciels non nécessaires
	* Le logiciel ou l’os devient plus le lent : bloatware entropique (le +)

#### Dette technique
Concept inventé par War Cunningham en 1992 pour expliquer l’implication d’une complexité technique, un code mal conçu requiert le paiement d’intérêts (en termes de maintenance, d’effort d’évolution ou de travail pour nettoyer le code) dans un délai plus ou moins long. 
→ d’après les auteurs, on peut étendre ces intérêts à des intérêts écologiques : les pratiques quick and dirty induisent plus de conso d’énergie, obsolescence accélérée, obésiciel... 

